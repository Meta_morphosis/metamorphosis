# What is the value you can get?
This is an overview about a huge part of my idea landscape, documented using a principle called Zettelkasten.
You can find all information about the vision of network societies, practices for collective intelligence and 
how to make a difference for societal organization in the 21th century taking concrete action. 
Let's get inspired and contact me for collaboration;)
- Telegram: @aeonjosh
- LinkedIn Aeon Joshua Hochberg
- Email: aeon.hochberg@posteo.de
- twitter: @AeonHochberg
- YouTube: 

# Learn about the method and use Zettelkasten 

Definition
A Zettelkasten (die Zettelkästen in plural) is a methodology for thinking in writing. It combines note taking while reading, personal thoughts and a bibliographic system.
It's an approach for a tool working more like the human brain, being processor and storage in once.


# Intro Material

- https://strengejacke.files.wordpress.com/2015/10/introduction-into-luhmanns-zettelkasten-thinking.pdf 
- http://www.dansheffler.com/blog/2015-05-05-the-zettelkasten-method/
- http://www.zettelkasten.danielluedecke.de/doc.php (java desktop programm) 
- https://www.lesswrong.com/posts/NfdHG6oHBJ8Qxc26s/the-zettelkasten-method-1
- Zettelkasten app in verwendung: https://www.youtube.com/watch?v=T7zdyyTGPaI https://twitter.com/ctietze
- Erfahrungsbericht von obiger App hinzu notion: https://forum.zettelkasten.de/discussion/404/moving-on
- die Autoren des Blogs haben auch ein Buch geschrieben: https://zettelkasten.de/book/de/
- andere Tools: https://zettelkasten.de/tools/

# Zettelkasten methode (Niklas Luhmann)

- via: https://www.youtube.com/watch?v=4veq2i3teVk and https://www.youtube.com/watch?v=NbncA7bDl70
- entscheidend ist was aus dem gelesenen für das vorhandene verwendbar ist
- im vordergrund steht nicht die letzliche vollendeung des aktuellen zettels/gedankens sondern das sich das was auf dem zettel steht erst im kontext anderer zettel herausstellt !
- Wert eines zettels erst durch die verbindung!
- Grothendieck learning metapher
- einziges kriterium für folgezettel: er sollte an den vorhergehenden passen? 
- Themen sind durch multiple storage, zwangsläufig in verschiedene  andere Themen eingebettet -> mehrfach bedeutung, Zukunftsoffenheit durch vermeidung von festgelegter Ordnung
- Klumpenbildung und schwarze Löcher => die Ordnung passt sich der Gedankenentwicklung an! (bietet somit eigentlich Potential zur Reflektion der Gedankenstruktur weil sie dadurchgreifbar, serialisiert ist)
- branching Nummerierung
- interne verweise
- 3 verweistypen 2- sammelverweise semantische zusammenhänge, verbindungen schaffen zwischen verwandten begriffen oder nur direkte wort nennung?
- Schlagwort register (tags) begriffsabbildung "relevanter" begriffe mit beispielhaftem verweis
- Zettelkasten ist ein Denkwerkzeug, eine Technik, jedoch primär bzw. wie Luhmann ihn verwandte zur **Selbst** Reflektion
